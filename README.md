# Proof Of Concept Software
Contains both Arduino code as well as MSP430 code
# MSP430
## Requirements
* The [TI MSP430 GCC toolchain](http://software-dl.ti.com/msp430/msp430_public_sw/mcu/msp430/MSPGCC/latest/index_FDS.html)
  * Visit link
  * Make TI account
  * Download the Full Installer
  * Note the installed directory
* `mspdebug`
* `build-essential` (`make`, etc...)

## Building
`make MSP430_TOOLCHAIN_PATH=/path/to/msp430-gcc`

## Flashing
`make flash MSP430_TOOLCHAIN_PATH=/path/to/msp430-gcc`

# Arduino
## Requirements
* You need to download/get [Arduino Makefile](https://github.com/sudar/Arduino-Makefile)
* When using the Makefile, you need to define
    * `ARDUINO_MAKEFILE`: The location of [Ardunio Makefile](https://github.com/sudar/Arduino-Makefile) (`/path/to/Arduino-Makefile/Arduino.mk`)
    * `MONITOR_PORT`: The port that the Arduino is connected to (`/dev/ttyUSB0`)

## Building
`make clean ARDUINO_MAKEFILE=blah`

`make all ARDUINO_MAKEFILE=blah`

## Flashing
`make upload ARDUINO_MAKEFILE=blah MONITOR_PORT=/dev/blah`

