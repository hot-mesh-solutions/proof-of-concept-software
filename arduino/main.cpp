#include <Arduino.h>
#include <HardwareSerial.h>
#include <RH_RF95.h>

#define TX_TIMEOUT (5000)
#define RX_TIMEOUT (15000)

#define BAUDRATE (115200)

char buf[255];
unsigned long lastSend = 0;
unsigned int sendDelay = 5000; // Delay in ms
bool packetWaiting;

void messageReceived() {
    packetWaiting = true;
}

#define SS (9)
#define RFM95_INT_PIN (3) // RFM95 Interrupt pin
#define RFM95_RST_PIN (2) // RFM95 Reset pin

RH_RF95 rf95_radio(SS, RFM95_INT_PIN);

void setup() {
    Serial.begin(BAUDRATE);
    while (!Serial) ; // Wait for serial port to be available

    Serial.println("Hot Mesh Solutions Proof of Concept");

    Serial.println("Using RFM95 firmware");
    pinMode(RFM95_RST_PIN, OUTPUT);
    digitalWrite(RFM95_RST_PIN, HIGH);

    // manual reset
    digitalWrite(RFM95_RST_PIN, LOW);
    delay(10);
    digitalWrite(RFM95_RST_PIN, HIGH);
    delay(10);

    // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
    while (!rf95_radio.init(true)) {
        Serial.println("RFM95 init failed");
        delay(2000);
    }
    delay(1000);
    rf95_radio.setModemConfig(rf95_radio.Bw125Cr45Sf128); // Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Default medium range
//    rf95_radio.setModemConfig(rf95_radio.Bw500Cr45Sf128); // Bw = 500 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on. Fast+short range
//    rf95_radio.setModemConfig(rf95_radio.Bw31_25Cr48Sf512); // Bw = 31.25 kHz, Cr = 4/8, Sf = 512chips/symbol, CRC on. Slow+long range
//    rf95_radio.setModemConfig(rf95_radio.Bw125Cr48Sf4096); // Bw = 125 kHz, Cr = 4/8, Sf = 4096chips/symbol, CRC on. Slow+long range

    rf95_radio.setFrequency(915.0);
    rf95_radio.setTxPower(11, false);
    rf95_radio.printRegisters();
    delay(500);
}

void loop() {
    uint8_t data[] = "Hi!";
    sprintf(buf, "Sending %s\r\n", data);
    Serial.print(buf);
    rf95_radio.send(data, sizeof(data));

    if (!rf95_radio.waitPacketSent(TX_TIMEOUT)) {
        Serial.println("TX Timeout");
        setup();
        return;
    }

    Serial.println("Waiting for reply...");
    if (rf95_radio.waitAvailableTimeout(RX_TIMEOUT)) {
        // Should be a reply message for us now
        uint8_t len = sizeof(buf);
        if (rf95_radio.recv(buf, &len)) {
            Serial.print("Got reply: ");
            Serial.println((char*)buf);
            Serial.print("RSSI: ");
            Serial.println(rf95_radio.lastRssi(), DEC);
        }
        else {
            Serial.println("Receive failed");
        }
    }
    else {
        Serial.println("RX Timeout");
        setup();
        return;
    }

    delay(500); // Delay before TX
}

