#ifndef CC1101_UTILS_H
#define CC1101_UTILS_H

void cc1101_easy_init(CC1101 radio, uint8_t *syncword, uint8_t frequency, uint8_t power);
void cc1101_dump_info(CC1101 radio, char buf[]);
int cc1101_rssi(char raw);
int cc1101_lqi(char raw);

#endif //CC1101_UTILS_H
